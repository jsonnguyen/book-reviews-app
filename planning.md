# Planning


* [ ] Make a virtual environment.
* [ ] Activate virtual environment.
* [ ] Install Django.
* [ ] Create a Django project
* [ ] Add a Django app to the project
* [ ] Start Django server
* [ ] Make some templates